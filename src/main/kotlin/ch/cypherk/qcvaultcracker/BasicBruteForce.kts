/**
 * Simple script to figure out the code of
 * https://qcvault.herokuapp.com/
 *
 * expected time for server-side validation: 5 sec
 *
 * expected attempt format:
 * POST to https://qcvault.herokuapp.com/unlock_safe
 * with JSON body like:
 * {
 *  first: code[0],
 *  second: code[1],
 *  third: code[2]
 * }
 *
 * expected response:
 * - "Wrong code" in response body if submission does not contain current code
 * - ??? otherwise
 * */

import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.async.Callback
import com.mashape.unirest.http.exceptions.UnirestException
import java.lang.IllegalStateException
import java.lang.RuntimeException
import kotlin.system.measureTimeMillis

val targetURL = "https://qcvault.herokuapp.com/unlock_safe"
val requestTemplate = "{\"first\":%d,\"second\":%d,\"third\":%d}"
val timeoutNanos = 20e9 //wait at most 20s

Unirest.setConcurrency(1000,1000)
Unirest.setDefaultHeader("Content-Type","application/json")

@Volatile var found = false

val elapsedMilis = measureTimeMillis {
    outer@
    for (a in 0..9) {
        for (b in 0..9) {
            for (c in 0..9) {
                if (found) break@outer
                Unirest.post(targetURL)
                    .body(String.format(requestTemplate, a, b, c))
                    .asStringAsync(object : Callback<String> {
                        override fun completed(response: HttpResponse<String>?) {
                            response?.let { handleResponse(it, a, b, c) }
                                ?: throw IllegalStateException("null response")
                        }

                        override fun cancelled() {}
                        override fun failed(e: UnirestException?) {
                            throw RuntimeException(e)
                        }
                    })
            }
        }
    }

    val start = System.nanoTime()
    while (!found && (System.nanoTime() - start < timeoutNanos)) {}
}
Unirest.shutdown()
if(found)println("found solution in: ${elapsedMilis/1000.0}s")
else println("FAILED")

fun handleResponse(response: HttpResponse<String>,a:Int,b:Int, c:Int){
    if(response.body != "Wrong code"){
        println("SOLUTION: $a $b $c")
        found = true
    }
}