# QC Vault Cracker

A solution to https://qcvault.herokuapp.com/ written in Kotlin.

## Assumptions

### Request Format

```
POST to https://qcvault.herokuapp.com/unlock_safe
with JSON body like:
{
"first":code[0],
"second":code[1],
"third":code[2]
}
```

### Server-side validation

takes about 5 seconds

### Client-side validation

A response that doesn't read `Wrong code` in its body is assumed to pass. This is in line with the client-side check the website performs:

```javascript
if (xmlHttp.responseText === "Wrong code") document.getElementById("tryagain").style.display = "block";
```

## [BasicBruteForce.kts](https://bitbucket.org/CypherK/qcvault/src/master/src/main/kotlin/ch/cypherk/qcvaultcracker/BasicBruteForce.kts)

Uses [Unirest](http://unirest.io/java.html) to dispatch 1'000 requests, one for each possible combination, then asynchronously handles the responses.

Makes use of "traditional" for-loops such as to abort dispatch with a labelled break if a solution happens to be found  while it is still dispatching requests.

After dispatch, waits for at most 20 seconds. Since server-side validation is expected to take 5 seconds, that should be plenty to account for latency and local processing. If during that time, no response arrives that indicates that the corresponding code was accepted, the attempt is considered failed.

If the code happens to be changed in the middle of the running attempt, the attempt may

* fail because the new valid code was already tested and rejected prior to the code change
* incorrectly print two "solutions" because responses for both the old code and the new had been dispatched and handled 
* print a wrong "solution" because the old code had already been accepted but the script not yet shut down

The attempt does not try to account for these cases.